import type {Config} from '@stencil/core';

export const config: Config = {
  namespace: 'calipsa',
  srcDir: '.',
  buildEs5: false,
  outputTargets: [
    {
      type: 'www',
      dir: 'dist/gui/stencil'
    },
  ],
  hashFileNames: false,
  minifyJs: false,
  minifyCss: false,
  plugins: [],
  extras: {
    // disable support for legacy browsers
    cssVarsShim: false,
    dynamicImportShim: false,
    safari10: false,
    scriptDataOpts: false,
    shadowDomShim: false
  },
};
