# Calipsa coding challenge

## Installation

First, install Node.js 16.5.0. Then run the following commands in the root directory:

```
npm run copy-common
npm run build-basic
npm run build-stencil
```

The second command builds both the server and the no-framework version of the front-end;
the third one builds the Stencil front-end.

## Running

Start the server with the command 

```
npm start
```

Then in the browser call up either `http://localhost:9000/noframework/` (basic version) or 
`http://localhost:9000/stencil/` (Stencil version).

## Testing

You can run the tests from the command line using the command

````
npm test
````

Alternatively you can run them within WebStorm 2021 with this configuration (you can 
either copy it into your `workspace.xml` or create a matching configuration using the 
"Edit Configurations..." GUI):

```xml
<component name="RunManager">
    <configuration name="All tests" type="JavaScriptTestRunnerJest">
      <config-file value="$PROJECT_DIR$/jest.config.cjs" />
      <node-interpreter value="project" />
      <node-options value="--experimental-vm-modules --no-warnings" />
      <jest-package value="$PROJECT_DIR$/node_modules/jest" />
      <working-dir value="$PROJECT_DIR$" />
      <envs />
      <scope-kind value="ALL" />
      <method v="2" />
    </configuration>
</component>
```

## Features

The entire project uses Typescript and ESM modules. I believe that Typescript and modern
ESM are a must nowadays.

### Back-end

I believe this submission meets all the requirements of the back-end and then some (it has
extensive unit tests and intelligent caching). Authentication is token-based. The order flow
is `main` &rarr; `app` &rarr; `apiCache` &rarr; `api`; the `api` module is a low-level one 
that does not implement pagination.

The `api` module could have used an in-memory database such as `@databases/sqlite` (a stand-alone 
database would be totally overkill for this coding challenge), but the immutable data here were 
simple enough that the code needed to implement the desired selections was still reasonably small. 

### Front-end

The front-end comes in two versions: one that doesn't use Vue or any other framework and one
that uses [Stencil](https://stenciljs.com/) and hence TSX. Both are quite fully-featured
(try typing incorrect input in the form fields, for instance). 

In the Stencil version individual components are the equivalent of Vue single-file components 
with class-based syntax and render methods while [Stencil Store](https://stenciljs.com/docs/stencil-store)
is the equivalent of Vuex. In Stencil another option (not used here) for setting up reactive 
state is the [`@State()`](https://stenciljs.com/docs/state) decorator: I believe that would 
be the equivalent of a Vue `ref`. Architecturally, Vue and Stencil are therefore not too
dissimilar; in addition there is even a Vue [integration](https://stenciljs.com/docs/vue) 
for Stencil.

Missing in all versions are the plots as that would really have taken too much time; in a 
real-world project I would probably have used [Plotly](https://plotly.com/javascript/) for this
(D3 &ndash; which Plotly is built on top of &ndash; is very low-level and quite a pain to use, 
especially with Typescript &ndash; horrific types). 

The front-end eschews calendar widgets for the datetime fields because the native ones are frankly 
a bit of a mess, and I have yet to find a third-party one I really like (flatpickr is pretty 
good but has some issues with ranges). In any case the way I did it has a major advantage: you 
can copy-paste datetime strings from the data on the left, which is very convenient for testing.

Likewise, for simplicity the front-end does not use any CSS framework (I am most familiar with
bootstrap but not terribly keen on it).

Lastly, I must say that I did not understand what you mean by

> Allow the user to view data about individual entries in detail

The individual entries in this case (alarms) are so small that one struggles to imagine
what a detail view would look like.

## Timing

I spent way more than 2 hours on this, on and off, because I turned it into an opportunity 
to learn about Jest and Express; I had fun with the GUI, too (I like GUI work). I believe 2 
hours is wildly unrealistic in any case: 4 hours would be more like it (test are always a 
time sink) and that only _if_ you do a single front-end version and are already very familiar
with all the frameworks used, client- or server-side; you would still have to skip the plots,
though.

## Remarks

The supplied data include a funky closing brace in location names. I believe this to be an error
during data preparation, and my program removes it when loading the data.
