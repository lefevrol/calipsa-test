"use strict"

// NB: This file needs to be under gui and not src since it will be imported in the browser.

/**
 * Enable or disable element.
 */
export const setEnabled = (elt: HTMLElement, enabled: boolean) => {
    if (enabled) {
        elt.removeAttribute("disabled")
    } else {
        elt.setAttribute("disabled", "disabled")
    }
}

/**
 * Add or remove class.
 */
export const setClassEnabled = (elt: HTMLElement, className: string, enabled: boolean) => {
    if (enabled) {
        elt.classList.add(className)
    } else {
        elt.classList.remove(className)
    }
}
