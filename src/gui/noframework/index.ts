"use strict";

import type {Alarm, Results} from "../../server/apiTypes";
import {setClassEnabled, setEnabled} from "./guiUtils";

(() => {

    enum PageType {
        FIRST = "first", PREVIOUS = "previous", CURRENT = "current", NEXT = "next", LAST = "last"
    }

    let pageSizeInput: HTMLInputElement
    let startInput: HTMLInputElement
    let endInput: HTMLInputElement
    let outcomeButtons: HTMLInputElement[]
    let pageDiv: HTMLElement
    const paginationButtons: Map<PageType, HTMLElement> = new Map()
    let alarmList: HTMLElement
    let errorField: HTMLElement

    let pageCount: number | undefined

    window.onload = () => {
        console.log("onload")

        // non-button elements
        startInput = document.getElementById("start")! as HTMLInputElement
        endInput = document.getElementById("end")! as HTMLInputElement
        outcomeButtons = Array.from(document.querySelectorAll("input[type='radio']"))
        pageDiv = document.getElementById("page")!
        pageSizeInput = document.getElementById("pageSize")! as HTMLInputElement
        alarmList = document.getElementById("alarms")!
        errorField = document.getElementById("error")!

        // buttons or button-like elements
        Object.values(PageType).forEach(registerClickListener);

        // monitor changes in text inputs
        [pageSizeInput, startInput, endInput].forEach(registerInputListener);

        fixTooltips()
    }

    const fixTooltips = () => {
        // fix up info tooltip; 1) \n or even \u000a breaks in the HTML string would be taken literally
        // and have to be introduced here instead 2) remove excessive space from multi-line strings
        document.querySelectorAll("img.tip").forEach(elt => {
            let title = elt.getAttribute("title")!
            elt.setAttribute("title", title.replaceAll(/\n\s+|\|/g, x => x === "|" ? "\n" : " "))
        })
    }

    const registerInputListener = (elt: HTMLInputElement) => {
        let rx = RegExp(elt.pattern)
        elt.addEventListener("input", () => {
            let ok = (elt.value === "" && !elt.required) || rx.test(elt.value)
            //console.log(elt.id, elt.required, elt.value, ok)
            setClassEnabled(elt, "invalid", !ok)
        })
    }

    const registerClickListener = (ptype: PageType) => {
        let elt = document.getElementById(ptype)!
        elt.addEventListener("click", () => getPage(ptype))
        paginationButtons.set(ptype, elt)
    }

    const updatePaginationButtons = (pageCount: number, page: number) => {
        setEnabled(paginationButtons.get(PageType.FIRST)!, page != 0)
        setEnabled(paginationButtons.get(PageType.PREVIOUS)!, page > 0)
        setEnabled(paginationButtons.get(PageType.NEXT)!, page < pageCount - 1)
        setEnabled(paginationButtons.get(PageType.LAST)!, page != pageCount - 1)
    }

    // NB: The on-screen page index has origin 1 for user convenience.

    const getPage = (ptype: PageType) => {
        console.log("getPage", ptype)
        let page = getFuturePage(ptype)
        fetch(`/api?${getQueryParams(page)}`, {
            method: "GET", headers: {
                "Accept": "application/json;q=0.9,text/plain",
                "Authorization": "Bearer jr688QwTIc5WA1N3sitC",
            }
        })
            .then(resp => resp.ok ? resp.json() : null)
            .then((results: Results) => {
                console.log("results", results)
                if (results != null) {
                    let alarms = results.alarms
                    alarmList.innerHTML = alarms.length === 0 ? "" : createAlarmsList(alarms)
                    pageDiv.textContent = (results.page + 1).toString()
                    pageCount = results.pageCount
                    updatePaginationButtons(pageCount, page)
                    errorField.textContent = null
                } else {
                    errorField.textContent = "An error occurred"
                }
            })
            .catch(err => console.error(err))
    }

    const getFuturePage = (ptype: PageType): number => {
        let pageStr = pageDiv.textContent
        let page = pageStr === "" ? 0 : parseInt(pageStr!) - 1
        return ptype === PageType.FIRST ? 0 : ptype === PageType.PREVIOUS ? page -1 :
            ptype === PageType.CURRENT ? page : ptype === PageType.NEXT ? page + 1 : pageCount! - 1;
    }

    const getQueryParams = (page: number): string => {
        let pageSize = pageSizeInput.value
        let start = startInput!.value
        let end = endInput!.value
        let outcome = outcomeButtons.filter(x => x.checked)[0].value

        let params = new URLSearchParams()
        params.append("pageSize", pageSize)
        params.append("page", page.toString())
        if (start !== "") params.append("start", fixDate(start))
        if (end !== "") params.append("end", fixDate(end))
        if (outcome !== "") params.append("outcome", outcome)
        return params.toString()
    }

    // NB: For the convenience of users the GUI format is 2yyyy-MM-dd hh:mm:ss.SSS" and not
    //   "yyyy-MM-ddThh:mm:ss.SSSZ", i.e., the full ISO. Also, we trust our <input> pattern
    //   and don't re-check the value here; the server will of course check them.

    const fixDate = (dt: string): string =>
        dt === "" ? dt : dt.replace(" ", "T") + (dt.endsWith("Z") ? "" : "Z")

    const createAlarmsList = (alarms: Alarm[]): string => {
        let html = alarms.reduce((html: string, alarm: Alarm) => {
            html += `<li class="${alarm.outcome ? 'positive' : 'negative'}" data-index="${alarm.index}">`
            html += `${alarm.location}<br>${alarm.timestamp}`
            html += "</li>\n"
            return html
        }, "")
        return `<ul>${html}</ul>`
    }
})()
