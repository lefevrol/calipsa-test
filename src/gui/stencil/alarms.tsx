import {Component, Host, State, getAssetPath, h} from '@stencil/core'

import state from "./store"
import type {Alarm} from "../../server/apiTypes";

@Component({
    tag: "calipsa-alarms",
    styleUrl: "alarms.css",
    assetsDirs: ["../images"],
    shadow: true
})
export class Alarms {

    // hack to make the store work
    @State() alarms: Alarm[] = []

    imgDir = getAssetPath("../images");

    render() {
        let alarms = state.alarms
        console.log("render", "alarms", alarms.length)

        return (
            alarms.length === 0 ? null :
                <Host style={{
                    "--positive-image": `url(${this.imgDir}/green.svg)`,
                    "--negative-image": `url(${this.imgDir}/red.svg)`,
                }}>
                    <ul>
                        {alarms.map(alarm =>
                            <li class={`${alarm.outcome ? 'positive' : 'negative'}`} data-index={`${alarm.index}`}>
                                {alarm.location}<br/>{alarm.timestamp}
                            </li>
                        )}
                    </ul>
                </Host>
        )
    }
}
