import {Component, Element, getAssetPath, h} from '@stencil/core'

import {setClassEnabled, setEnabled} from "../noframework/guiUtils";
import type {Results} from "../../server/apiTypes";
import state from "./store"
import {capitalize} from "./utils";

const outcomes = {
    Positive: "true",
    Negative: "false",
    Any: ""
}

type OutcomeType = keyof typeof outcomes

enum PageType {
    FIRST = "first",
    PREVIOUS = "previous",
    CURRENT = "current",
    NEXT = "next",
    LAST = "last"
}

enum DateType {
    START = "start",
    END = "end"
}

@Component({
    tag: "calipsa-controls",
    styleUrl: "controls.css",
    assetsDirs: ["../images"],
    shadow: true
})
export class Controls {

    @Element() elt?: HTMLElement;
    private pageSizeInput?: HTMLInputElement
    private readonly dateInputs: Map<DateType, HTMLInputElement> = new Map()
    private pageLabel?: HTMLLabelElement
    private pageInput?: HTMLInputElement
    private readonly outcomeButtons: Map<OutcomeType, HTMLInputElement> = new Map()
    private readonly paginationButtons: Map<PageType, HTMLElement> = new Map()
    private errorField?: HTMLDivElement

    private readonly inputPatterns: Map<string, RegExp> = new Map()
    private pageCount: number | undefined
    private page = 0

    private updatePaginatorButtons = (pageCount: number, page: number) => {
        setEnabled(this.paginationButtons.get(PageType.FIRST)!, page != 0)
        setEnabled(this.paginationButtons.get(PageType.PREVIOUS)!, page > 0)
        setEnabled(this.pageInput!, pageCount != null)
        setEnabled(this.paginationButtons.get(PageType.NEXT)!, page < pageCount - 1)
        setEnabled(this.paginationButtons.get(PageType.LAST)!, page != pageCount - 1)
    }

    // NB: The on-screen page index has origin 1 for user convenience.

    private submit = (page: number) => {
        console.log("submitForm", page)
        fetch(`/api?${this.getQueryParams(page)}`, {
            method: "GET", headers: {
                "Accept": "application/json;q=0.9,text/plain",
                "Authorization": "Bearer jr688QwTIc5WA1N3sitC",
            }
        })
            .then(resp => resp.ok ? resp.json() : null)
            .then((results: Results) => {
                console.log("results", results)
                if (results == null || results.page !== page) {
                    this.errorField!.textContent = "An error occurred"
                } else {
                    state.alarms = results.alarms
                    this.pageInput!.value = (page + 1).toString()
                    this.pageCount = results.pageCount
                    this.pageLabel!.textContent = `Page (of ${results.pageCount})`
                    this.page = page
                    this.updatePaginatorButtons(this.pageCount, page)
                    this.errorField!.textContent = null
                }
            })
            .catch(err => console.error(err))
    }

    private getPageParam = (ptype: PageType): number => {
        return ptype === PageType.FIRST ? 0 : ptype === PageType.PREVIOUS ? this.page -1 :
            ptype === PageType.CURRENT ? this.page : ptype === PageType.NEXT ? this.page + 1 : this.pageCount! - 1;
    }

    private getQueryParams = (page: number): string => {
        let pageSize = this.pageSizeInput!.value
        let start = this.dateInputs.get(DateType.START)!.value
        let end = this.dateInputs.get(DateType.END)!.value
        let outcome = Array.from(this.outcomeButtons.values()).filter(x => x.checked)[0].value

        let params = new URLSearchParams()
        params.append("pageSize", pageSize)
        params.append("page", page.toString())
        if (start != "") params.append("start", this.fixDate(start))
        if (end != "") params.append("end", this.fixDate(end))
        if (outcome != "") params.append("outcome", outcome)
        return params.toString()
    }

    // NB: For the convenience of users the GUI format is 2yyyy-MM-dd hh:mm:ss.SSS" and not
    //   "yyyy-MM-ddThh:mm:ss.SSSZ", i.e., the full ISO. Also, we trust our <input> pattern
    //   and don't re-check the value here; the server will of course check them.

    private fixDate = (dt: string): string =>
        dt === "" ? dt : dt.replace(" ", "T") + (dt.endsWith("Z") ? "" : "Z")

    // NB: \n or even \u000a breaks in the HTML tooltip string would be taken literally
    //   and have to be introduced here instead.

    private fixTooltip = (x: string): string =>
        x.replaceAll(/\|/g, "\n")

    private setInputRegex = (elt: HTMLInputElement) => {
        this.inputPatterns.set(elt.id, RegExp(elt.pattern))
    }

    private onInput = (e: Event) => {
        let elt = e.target as HTMLInputElement
        let rx = RegExp(elt.pattern)
        let ok = (elt.value === "" && !elt.required) || rx.test(elt.value)
        if (ok && elt === this.pageInput) {
            let newPage = parseInt(elt.value) - 1
            ok = newPage >= 0 && newPage < this.pageCount!
        }
        setClassEnabled(elt, "invalid", !ok)
    }

    private onKeyUp = (e: KeyboardEvent) => {
        if (e.code === "Enter" && !(e.target as HTMLInputElement)!.disabled) {
            this.submit(parseInt(this.pageInput!.value) - 1)
        }
    }

    render() {
        console.log("render", "controls")
        const imgDir = getAssetPath("../images");

        return (
            <div id="controls">
                <label htmlFor="pageSize">Items per page</label>
                <input id="pageSize" name="pageSize" type="text" value="50" placeholder="50" pattern="^\d+$" required
                       ref={ref => { this.pageSizeInput = ref; this.setInputRegex(ref!); } } onInput={this.onInput}/>
                <div></div>
                {Object.values(DateType).map(value =>
                    [
                        <label htmlFor="start">{capitalize(value)} time</label>,
                        <input id="start" name={`${value}`} type="text" placeholder="yyyy-MM-dd hh:mm:ss.SSS" size={24}
                               pattern="^(\d{4})-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])[T ]([01]\d|1[0-9]|2[0-3]):([0-5]\d)(:[0-5]\d)?(.\d{3})?Z?$"
                               ref={ref => { this.dateInputs.set(value, ref!); this.setInputRegex(ref!); } }
                               onInput={this.onInput}/>,
                        <img class="tip" src={`${imgDir}/info.svg`} alt="Tip" title={this.fixTooltip("The hint is " +
                            "slightly misleading in that the|field also accepts full ISO strings, i.e, with|T and Z " +
                            "but optional ss.ssS part. Thus you|can copy/paste from the alarms on the left.")}/>
                    ]
                )}
                <label htmlFor="outcomes">Outcome</label>
                <div id="outcomes">
                    {Object.entries(outcomes).map(e =>
                        [
                            <input name="outcome" type="radio" value={`${e[1]}`} checked={e[1] === ""}
                                   ref={ref => this.outcomeButtons.set(e[0] as OutcomeType, ref!)}/>,
                            e[0]
                        ]
                    )}
                </div>
                <div></div>
                <button id="current" type="button" onClick={() => this.submit(this.page)}>Submit</button>
                <div id="error" ref={ref => this.errorField = ref}></div>
                <div></div>
                <label htmlFor="paginator" ref={ref => this.pageLabel = ref!}>Page</label>
                <div id="paginator">
                    {Object.values(PageType).map(value =>
                        value === "current"
                            ? <input type="text" id="page" title="Current page; change it and press Enter"
                                     pattern="^[1-9]\d*$" required={false} disabled={true}
                                     ref={ref => this.pageInput = ref!}
                                     onInput={this.onInput} onKeyUp={this.onKeyUp}/>
                            : <input type="image" id={`${value}`} disabled={true}
                                     src={`${imgDir}/${value}.svg`}
                                     alt={`Go to ${value} page`} title={`Go to ${value} page`}
                                     ref={ref => this.paginationButtons.set(value, ref!)}
                                     onClick={() => this.submit(this.getPageParam(value))}/>
                    )}
                </div>
                <div></div>
            </div>
        )
    }
}
