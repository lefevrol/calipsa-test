"use strict"

export const capitalize = (x: string): string => x.replace(/\b\w/g, c => c.toUpperCase())
