import {createStore} from "@stencil/store";
import type {Alarm} from "../../server/apiTypes";

const {state} = createStore<{alarms: Alarm[]}>({alarms: []});

declare global {
    interface Window {
        state: any; any: any
    }
}

window["state"] = state

export default state
