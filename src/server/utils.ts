"use strict"

export class InvalidArgumentException extends Error {

    // noinspection JSUnusedGlobalSymbols
    code = "ERR_INVALID_ARG_VALUE"

    constructor(message?: string) {
        super(message)
    }
}


export const parseBoolean = (x: string): boolean | null | undefined =>
    x == null ? undefined : x === "true" ? true : x === "false" ? false : null

export const parseNumber = (x: string): number | null | undefined => {
    try {
        return x == null ? undefined : Number(x)
    } catch (e) {
        return null
    }
}

export const parseDate = (x: string): Date | null | undefined => {
    if (x == null) {
        return undefined
    } else {
        let dt = new Date(x)
        return isNaN(dt.getTime()) ? null : dt
    }
}
