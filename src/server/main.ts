"use strict"

import {app} from "./app";

const port = 9000

app.listen(port, () => {
    console.log(`Calipsa app listening at http://localhost:${port}`)
})
