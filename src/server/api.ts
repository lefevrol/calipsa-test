/**
 * Data loading and querying; in effect a poor man's in-memory database.
 */

"use strict"

import {readFile} from 'fs/promises'
import search from "binary-search"
import {URL} from "url"

import {appLogger} from "./config";
import {InvalidArgumentException} from "./utils"
import type {Alarm, RawData, Location, Timestamped} from "./apiTypes"

// string comparator; like the C strcmp()
const strCmp = (s1: string, s2: string): number => s1 < s2 ? -1 : s1 === s2 ? 0 : 1

// alarm comparator; the ISO UTC strings are such that we can sort them directly, i.e.,
// without converting them to Date objects first
const alarmCmp = (ts1: Timestamped, ts2: Timestamped): number => strCmp(ts1.timestamp, ts2.timestamp)

// remove funky brace (surely an error in data preparation) at the end of location names
const fixLocation = (x: string): string => x.endsWith("}") ? x.slice(0, x.length - 1) : x

// NB: Assuming well-formed data; in the real world one should probably not trust even internal
//   data and instead define a JSON schema and check the data against it using, e.g., Ajv.

// NB: Reading the JSON file instead of importing it to avoid being left with an unwanted copy
//   of the original data in the import cache (waste of space at the very least). Deliberately
//   not catching reading errors so as to simplify business logic; a failure to load the JSON
//   data is in any case terminal.

// NB: Fusing locations and alarms so the internal data already have the desired shape for serving.

let dataUrl = new URL("../resources/data.json", import.meta.url)
console.log("loading data from", dataUrl.href)
const alarms: Alarm[] = await readFile(dataUrl)
    .then(buf => JSON.parse(buf.toString()) as RawData)
    .then(json => {
        let locationMap = json.locations.reduce(
            (res: Map<number, string>, location: Location) => res.set(location.id, location.name),
            new Map()
        )
        // sort the alarms by timestamp to enable efficient filtering and enrich with index and location name
        return json.alarms.sort(alarmCmp)
            .map((x, i) => {
                return {
                    index: i,
                    location: fixLocation(locationMap.get(x.location) || ""),
                    timestamp: x.timestamp,
                    outcome: x.outcome
                } as Alarm
            })
    })

// NB: Taking start and end parameters of Date and not string type so as not to have to worry
//   about malformed datetime strings: that is not the point of this module.

/**
 * Select alarms by timestamp range and outcome.
 * @param {Date} [start] - the start date; optional
 * @param {Date} [end] - the end data; optional
 * @param {boolean} [outcome] - the outcome; optional
 * @returns {Alarm[]} array of Alarm objects, where the timestamps are UTC ISO strings
 * @throws {InvalidArgumentException} if start > end
 */
export const select = (start?: Date, end?: Date, outcome?: boolean): Alarm[] => {
    appLogger.debug("select %s %s %s", start, end, outcome)
    if (start != null && end != null && start > end) {
        throw new InvalidArgumentException("start > end")
    }
    let res = alarms
    // start index, inclusive
    let is = 0
    if (start != null) {
        let ts = start.toISOString()
        // bogus alarm for querying to avoid needing two comparators; only the timestamp matters
        is = search(res, {timestamp: ts}, alarmCmp)
        if (is === -res.length - 1) {
            // start falls after the last alarm
            return []
        } else if (is < 0) {
            // no match; taking the nearest alarm above
            is = -is - 1
        }
        // the insertion point is ill-defined in case of a stretch of identical timestamps
        while (is > 0 && res[is - 1].timestamp === ts) {
            is--
        }
    }
    if (is > 0) {
        // minor optimization for the next step
        res = res.slice(is)
    }
    // because of the slicing above ie is now relative to is
    let ie = res.length - 1
    if (end != null && end === start) {
        ie = 0
    } else if (end != null) {
        let ts = end.toISOString()
        // bogus alarm for querying to avoid needing two comparators; only the timestamp matters
        ie = search(res, {timestamp: ts}, alarmCmp)
        if (ie === -1) {
            // end falls before the first alarm
            return []
        } else if (ie < 0) {
            // no match; taking the nearest alarm below
            ie = -ie - 2
        }
        // the insertion point is ill-defined in case of a stretch of identical timestamps
        while (ie < res.length - 1 && res[ie + 1].timestamp === ts) {
            ie++
        }
    }
    if (ie < res.length - 1) {
        res = res.slice(0, ie + 1)
    }
    if (outcome != null) {
        res = res.filter(x => x.outcome === outcome)
    }
    return res
}

/**
 * Select alarms by index range.
 * @param {number} first - the first index, inclusive
 * @param {number} last - the last index, inclusive
 * @returns {Alarm[]} array of Alarm objects, where the timestamps are UTC ISO strings
 * @throws {InvalidArgumentException} if first > last
 */
export const slice = (first: number, last: number): Alarm[] => {
    appLogger.debug("slice %s %s", first, last)
    if (first > last) {
        throw new InvalidArgumentException("first > last")
    }
    return alarms.slice(first, last)
}
