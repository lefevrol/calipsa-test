/**
 * Data querying; this is the real API entry point and where pagination takes place.
 */

"use strict"

import NodeCache from "node-cache";

import type {Results} from "./apiTypes";
import {select, slice} from "./api";
import {appLogger} from "./config";
import {InvalidArgumentException} from "./utils";

// map of stringified query parameters to array of page first and last indices
const cache: NodeCache = new NodeCache({stdTTL: 60, checkperiod: 30, useClones: false})

/**
 * Get results from cache, with pagination; the TTL is 1h.
 * @param {number} pageSize - the number of item in a page
 * @param {number} page - the page index
 * @param {Date} [start] - the start date; optional
 * @param {Date} [end] - the end data; optional
 * @param {boolean} [outcome] - the outcome; optional
 * @returns {Results} object with properties
 *     page: the page index (as above)
 *     pageCount: the total number of pages
 *     cacheHit: flag indicating whether the cache was hit (for testing only)
 *     alarms: array of Alarm objects, where the timestamps are UTC ISO strings
 * @throws {InvalidArgumentException} if the page index is too high
 */
export const get = (pageSize: number, page: number, start?: Date, end?: Date, outcome?: boolean): Results => {
    appLogger.debug("get %s %s %s %s %s", pageSize, page, start, end, outcome)
    if (pageSize <= 0 || (pageSize % 1) !== 0) {
        throw new InvalidArgumentException(`Invalid page size: ${pageSize}`)
    }
    if (page < 0 || (page % 1) !== 0) {
        throw new InvalidArgumentException(`Invalid page: ${page}`)
    }
    let key = `${toString(start)} | ${toString(end)} | ${outcome} | ${pageSize}`
    let val = cache.get<number[]>(key)
    let hit = val != null
    if (val == null) {
        let res = select(start, end, outcome)
        if (res.length === 0) {
            val = []
        } else {
            val = res.filter((x, i) => (i % pageSize) === 0).map(x => x.index)
            if (0 !== (res.length / pageSize)) {
                val.push(res[res.length - 1].index + 1)
            }
        }
        cache.set(key, val)
    }
    let pageCount = Math.max(0, val.length - 1)
    appLogger.debug("page: %s, pageCount: %s, val: %s", page, pageCount, val)
    if (pageCount > 0 ? page > pageCount - 1 : page > 0) {
        throw new InvalidArgumentException(`Page index too high: ${page}`)
    }
    let alarms = pageCount === 0 ? [] : slice(val[page], val[page + 1])
    return {
        page: page,
        pageCount: pageCount,
        cacheHit: hit,
        alarms: outcome == null ? alarms : alarms.filter(x => x.outcome === outcome)
    } as Results
}

const toString = (dt?: Date): string => dt == null ? "null" : dt.toISOString()
