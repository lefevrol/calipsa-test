"use strict"

import appRoot from "app-root-path";
import express from "express"
import morgan from "morgan";
import path from "path";
import type {StreamOptions} from "morgan";

import {appLogger} from "./config";
import {parseBoolean, parseNumber, parseDate, InvalidArgumentException} from "./utils";
import {get} from "./apiCache";

const app = express()

// NB: TS requires imports to have no extension or a .js extension. In the case of our own code the latter
//   would create a bootstrapping issue while the former cause the imports to lack a .js extension also in
//   the transpiled JS files. With node this can be finessed with the --experimental-specifier-resolution=node
//   option but web servers need the actual file, hence the URL rewriting (which must come first in the
//   middleware list).

app.use(function(req, resp, next) {
    //console.log("url", req.url)
    if (req.url.endsWith("guiUtils")) {
        req.url += ".js"
    }
    next();
});

app.use("/static", express.static(`${appRoot}/dist`,))

const stream: StreamOptions = {
    write: (message) => appLogger.info(message),
};

app.use(morgan("short", { stream }))

app.use(function (req, res, next) {
    var filename = path.basename(req.url);
    appLogger.debug(`requested ${filename}`)
    next();
});

app.get("/noframework/", (req, resp) => {
    resp.sendFile(`${appRoot}/dist/gui/noframework/index.html`)
})

app.get("/stencil/", (req, resp) => {
    resp.sendFile(`${appRoot}/dist/gui/stencil/index.html`)
})

app.get("/api/", (req, resp) => {
    appLogger.debug("request %s %s", req.query)
    // token-based authentication
    let auth = req.header("authorization") || req.header("Authorization")
    if (auth == null || !/^Bearer [a-zA-Z0-9]+$/.test(auth) || !authenticate(auth.slice(7))) {
        resp.sendStatus(401)
    }
    // query parsing
    let [pageSize, page, start, end, outcome] = [
        parseNumber(req.query.pageSize as string),
        parseNumber(req.query.page as string),
        parseDate(req.query.start as string),
        parseDate(req.query.end as string),
        parseBoolean(req.query.outcome as string)
    ]
    appLogger.debug("query args %s %s %s %s %s", pageSize, page, start, end, outcome)
    // by parseXxx convention null denotes an invalid query parameter
    if (pageSize === null || pageSize === undefined || page === null || page === undefined ||
        start === null || end === null || outcome === null) {
        resp.sendStatus(400)
    } else {
        try {
            let res = get(pageSize, page, start, end, outcome)
            resp.json(res)
        } catch (e) {
            resp.sendStatus(e instanceof InvalidArgumentException ? 400 : 500)
        }
    }
})

// in real-world use the token would be looked in a db, as there would be many tokens: one for each user
const authenticate = (token: string): boolean => token === "jr688QwTIc5WA1N3sitC"

export {app}
