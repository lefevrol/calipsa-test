"use strict"

import appRoot from "app-root-path";
import winston from "winston";
import DailyRotateFile from "winston-daily-rotate-file"

export const appLogger = winston.createLogger({
    transports: [
        new winston.transports.Console({
            level: "debug",
            handleExceptions: true,
            format: winston.format.combine(
                //winston.format.timestamp(),
                winston.format.splat(),
                winston.format.printf(({level, message, timestamp}) =>
                    `${message}`
                )
            )
        }),
        new winston.transports.DailyRotateFile({
            level: "info",
            handleExceptions: true,
            filename: `${appRoot}/logs/app.log`,
            maxFiles: 5,
            format: winston.format.combine(
                winston.format.timestamp(),
                winston.format.splat(),
                winston.format.printf(({level, message, timestamp}) =>
                    `${timestamp}\t${level}:\t${message}`
                )
            )
        }),
    ],
    exitOnError: false,
});
