interface Location {
    id: number,
    name: string,
}

export interface Timestamped {
    timestamp: string
}

export interface RawAlarm extends Timestamped {
    location: number
    outcome: boolean
}

export interface Alarm extends Timestamped {
    index: number
    location: string
    outcome: boolean
}

export interface RawData {
    locations: Location[]
    alarms: RawAlarm[]
}

export interface Results {
    cacheHit: boolean
    page: number
    pageCount: number
    alarms: Alarm[]
}
