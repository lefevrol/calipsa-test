import {beforeAll, afterAll, describe, expect, test} from "@jest/globals"
import "@matt-tingen/jest-macros/global"
import supertest from "supertest";
import type {Server} from "http";

import {app} from "../server/app"
import type {Results} from "../server/apiTypes";

const port = 9010

let server: Server

const headers = {"Authorization": "Bearer jr688QwTIc5WA1N3sitC"}

beforeAll(() => {
    server = app.listen(port, () => {
        console.log(`Calipsa app listening at http://localhost:${port}`)
    })
})

afterAll(() => {
    server.close()
})

let res_100000 = {
    "page": 0,
    "pageCount": 100000,
    "cacheHit": false,
    "alarms": [
        {
            "index": 0,
            "location": "location 10",
            "timestamp": "2021-01-02T00:00:26.363Z",
            "outcome": true
        },
    ]
}

let res_49819 = {...res_100000, pageCount: 49819}

let res_1 = {...res_100000, pageCount: 1}

let res_0 = {
    "page": 0,
    "pageCount": 0,
    "cacheHit": false,
    "alarms": [],
}

// some dates, sorted
let dates = [
    '2021-01-02T00:00:26.360Z',  // before first datum
    '2021-01-02T00:00:26.363Z',  // first datum
    '2021-01-02T00:00:26.365Z',  // after first datum
]

const testValid = (title: string, expected: Results, queryParams: string) => {
    test(title, (done) => {
        supertest(app)
            .get(`/api?${queryParams}`)
            .set(headers)
            .expect(200)
            .then(resp => {
                expect(resp.body).toEqual(expected)
                done()
            })
            .catch(err => done(err))
    });
}

describe("valid inputs", () => {
    testValid("empty", res_0, `pageSize=1&page=0&end=${dates[0]}`)
    testValid("no range", res_100000, "pageSize=1&page=0")
    testValid("no range, with outcome", res_49819, "pageSize=1&page=0&outcome=true")
    testValid("inexact upper timestamp", res_1, `pageSize=1&page=0&end=${dates[2]}`)
    testValid("inexact lower timestamp", res_100000, `pageSize=1&page=0&start=${dates[0]}`)
    testValid("inexact timestamps", res_1, `pageSize=1&page=0&start=${dates[0]}&end=${dates[2]}`)
    testValid("exact upper timestamp", res_1, `pageSize=1&page=0&end=${dates[1]}`)
    testValid("exact lower timestamp", res_100000, `pageSize=1&page=0&start=${dates[1]}`)
    testValid("exact timestamps", res_1, `pageSize=1&page=0&start=${dates[1]}&end=${dates[1]}`)
})

const testInvalid = (title: string, queryParams: string, authHeaders: object) => {
    test(title, (done) => {
        supertest(app)
            .get(`/api?${queryParams}`)
            .set(authHeaders)
            .expect(authHeaders === headers ? 400 : 401, done)
    });
}

describe("invalid inputs", () => {
    testInvalid("no auth", "pageSize=-1&page=2", {});
    testInvalid("malformed auth", "pageSize=-1&page=2", {"Authorization": "bzzt"});
    testInvalid("invalid token", "pageSize=-1&page=2", {"Authorization": "Bearer bzzt"});
    testInvalid("negative page size", "pageSize=-1&page=2", headers);
    testInvalid("negative page", "pageSize=1&page=-2", headers);
    testInvalid("undefined page size", "page=-2", headers);
    testInvalid("undefined page", "pageSize=1", headers);
    testInvalid("float page size", "pageSize=1.1&page=2", headers);
    testInvalid("float page", "pageSize=1&page=2.2", headers);
    testInvalid("invalid start", "pageSize=1&page=2&start=xyz", headers);
    testInvalid("invalid end", "pageSize=1&page=2&end=xyz", headers);
    testInvalid("invalid outcome", "pageSize=1&page=2&outcome=xyz", headers);
})
