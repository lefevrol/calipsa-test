"use strict"

import {describe, expect} from "@jest/globals"
import "@matt-tingen/jest-macros/global"

import {select, slice} from "../server/api"
import {get} from "../server/apiCache"
import {InvalidArgumentException} from "../server/utils"
import type {Alarm} from "../server/apiTypes";

// the size of the data
let totalSize = 100000

// some dates, sorted
let dates = new Map<string | undefined, Date>()
dates.set("first--", new Date('2021-01-02T00:00:26.357Z'))  // even earlier
dates.set("first-",  new Date('2021-01-02T00:00:26.360Z'))  // before first datum
dates.set("first",   new Date('2021-01-02T00:00:26.363Z'))  // first datum
dates.set("10-",     new Date('2021-01-02T00:04:54.915Z'))  // between #9 and #10
dates.set("10",      new Date('2021-01-02T00:04:54.917Z'))  // #10
dates.set("20",      new Date('2021-01-02T00:09:39.439Z'))  // #20
dates.set("20+",     new Date('2021-01-02T00:09:39.442Z'))  // between #20 and #21
dates.set("21",      new Date('2021-01-02T00:10:31.454Z'))  // #21
dates.set("last",    new Date('2021-02-01T23:59:56.339Z'))  // last datum
dates.set("last+",   new Date('2021-02-01T23:59:56.345Z'))  // after last datum
dates.set("last++",  new Date('2021-02-01T23:59:56.349Z'))  // even later

type optionalString = string | undefined
type optionalBoolean = boolean | undefined

// NB: By convention exact here means that the timestamp is in the data.

//===============================================================================================
//                                        Macros
//===============================================================================================

// NB: Jest support for exception testing (expect.toThrow method) is not the greatest...

// Base test harness for invalid inputs.
const testInvalid = createMacro(
    (func: () => void, message: string) => {
        expect.assertions(2)
        try {
            func()
        } catch (err) {
            expect(err).toBeInstanceOf(InvalidArgumentException)
            expect(err).toHaveProperty("message", message)
        }
    },
    title => title
)

// Base test harness for valid select inputs.
const testSelect = createMacro(
    /**
     * @param {string} [startKey] - the start date filter, identified by its dates key; optional
     * @param {string} [endKey] - the end date filter, identified by its dates key; optional
     * @param {boolean} [outcome] - the outcome filter; optional
     * @param {number} size - the number of alarms returned
     * @param {number} [firstIndex] - the index of the first result; optional if size = 0
     * @param {number} [lastIndex] - the index of the last result; optional if size = 0
     */
    (startKey: optionalString, endKey: optionalString, outcome: optionalBoolean,
     size: number, firstIndex?: number, lastIndex?: number) => {
        console.log(startKey, endKey, outcome, size, firstIndex, lastIndex)
        let res = select(dates.get(startKey), dates.get(endKey), outcome)
        checkResults(res, outcome, size, firstIndex, lastIndex)
     },
    title => title
)

// Base test harness for valid slice inputs.
const testSlice = createMacro(
    /**
     * @param {number} pageSize - as in {@link select}
     * @param {number} page - as in {@link select}
     * @param {string} [startKey] - the start date filter, identified by its dates key; optional
     * @param {string} [endKey] - the end date filter, identified by its dates key; optional
     * @param {boolean} [outcome] - the outcome filter; optional
     * @param {number} size - the number of alarms returned
     * @param {boolean} cacheHit - whether the query hit the cache
     * @param {number} [firstIndex] - the index of the first result; optional if size = 0
     * @param {number} [lastIndex] - the index of the last result; optional if size = 0
     *
     */
    (pageSize: number, page: number, startKey: optionalString, endKey: optionalString, outcome: optionalBoolean,
     size: number, cacheHit: boolean, firstIndex?: number, lastIndex?: number) => {
        console.log(pageSize, page, startKey, endKey, outcome, size, firstIndex, lastIndex)
        let res = get(pageSize, page, dates.get(startKey), dates.get(endKey), outcome)
        checkResults(res.alarms, outcome, size, firstIndex, lastIndex)
        expect(res.cacheHit).toBe(cacheHit)
    },
    title => title
)

const checkResults = (alarms: Alarm[], outcome: optionalBoolean, size: number, firstIndex?: number, lastIndex?: number) => {
    expect(alarms.length).toBe(size)
    if (size > 0) {
        // check first result
        expect(alarms[0].index).toBe(firstIndex)
        // checking last result
        expect(alarms[alarms.length - 1].index).toBe(lastIndex)
        // check that the joining of alarms to locations did not go awry
        expect(alarms.findIndex(x => x.location == null)).toBe(-1)
        if (outcome != null) {
            // check that all alarms have the desired outcome
            expect(alarms.findIndex(x => x.outcome === !outcome)).toBe(-1)
        }
    }
}

//===============================================================================================
//                                       api.ts tests
//===============================================================================================

run("invalid select date range",
    testInvalid, () => select(dates.get("10"), dates.get("10-")), "start > end"
)

run("invalid slice index range",
    testInvalid, () => slice(10, 9), "first > last"
)

const testSelectAll = (title: string, startKey: optionalString, endKey: optionalString) =>
    run(title, testSelect, startKey, endKey, undefined, totalSize, 0, totalSize - 1)

describe("select all data", () => {
    testSelectAll("no range", undefined, undefined)
    testSelectAll("fully exact range", "first", "last")
    testSelectAll("fully inexact range", "first-", "last+")
    testSelectAll("exact range open from above", "first", undefined)
    testSelectAll("inexact range open from above", "first-", undefined)
    testSelectAll("exact range open from below", undefined, "last")
    testSelectAll("inexact range open from below", undefined, "last+")
})

const testSelectOne = (title: string, outcome: optionalBoolean, size: number) =>
    run(title, testSelect, "10", "10", outcome, size, 10, 10)

describe("select single alarm", () => {
    testSelectOne("no outcome", undefined, 1)
    testSelectOne("outcome = false", false, 1)
})

const testSelectNone = (title: string, startKey?: string, endKey?: string, outcome?: boolean) =>
    run(title, testSelect, startKey, endKey, outcome, 0)

describe("select none", () => {
    testSelectNone("end < earliest", undefined, "first-")
    testSelectNone("start and end < earliest", "first--", "first-")
    testSelectNone("start > latest", "last+")
    testSelectNone("start and end > latest", "last+", "last++")
    testSelectNone("outcome not matched", "10", "10", true)
})

const testSelectSome = (title: string, startKey: string, endKey: string) =>
    run(title, testSelect, startKey, endKey, undefined, 11, 10, 20)

describe("select data without outcome filter", () => {
    testSelectSome("fully exact range", "10", "20")
    testSelectSome("inexact range from below", "10-", "20")
    testSelectSome("inexact range from above", "10", "20+")
    testSelectSome("fully inexact range", "10-", "20+")
})

describe("select data with outcome filter", () => {
    run("false outcome", testSelect, "10", "20", false, 7, 10, 20)
    run("true outcome", testSelect, "10", "20", true, 4, 11, 19)
})

//===============================================================================================
//                                     apiCache.ts tests
//===============================================================================================

describe("invalid cache inputs", () => {
    run("page index too high",
        testInvalid, () => get(Math.floor(totalSize / 10), 20), "Page index too high: 20"
    )
    run("negative page size",
        testInvalid, () => get(-4, 7), "Invalid page size: -4"
    )
    run("non-integer page size",
        testInvalid, () => get(4.1, 7), "Invalid page size: 4.1"
    )
    run("negative page index",
        testInvalid, () => get(4, -7), "Invalid page: -7"
    )
    run("non-integer page index",
        testInvalid, () => get(4, 7.1), "Invalid page: 7.1"
    )
})

describe("cache tests", () => {
    run("empty", testSlice, 20, 0, undefined, "first-", undefined, 0, false)
    run("all data on one page", testSlice, 20, 0, "10", "20", undefined, 11, false, 10, 20)
    run("4 0, 11 total", testSlice, 4, 0, "10", "20", undefined, 4, false, 10, 13)
    run("4 1, 11 total", testSlice, 4, 1, "10", "20", undefined, 4, true, 14, 17)
    run("4 2, 11 total", testSlice, 4, 2, "10", "20", undefined, 3, true, 18, 20)
    run("6 0, 12 total", testSlice, 6, 0, "10", "21", undefined, 6, false, 10, 15)
    run("6 1, 12 total", testSlice, 6, 1, "10", "21", undefined, 6, true, 16, 21)
})
